<?php



if ( ! function_exists( 'sn_theme' ) ) {
  function sn_theme() {
    add_theme_support( 'menus' );
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'sn_big', 888, 564, true );
    add_image_size( 'sn_normal', 432, 270, true );

  }
  add_action('after_setup_theme', 'sn_theme');
}


