<?php

global $idReady;
$idReady = [];

function allStickiesHome () {
	$stickies = [];

  // Get all Stickies
  $qSticky1 = array(
		'post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => 1,
		'meta_query'	=> array( 'relation' => 'OR', array( 'key' => 'sticky', 'value' => '1', 'compare' => 'LIKE' ), )
	);
	$sticky1 = get_posts($qSticky1);
	$GLOBALS['idReady'] = array_merge($GLOBALS['idReady'], wp_list_pluck( $sticky1, 'ID' ));
	$sticky1[0]->image = get_the_post_thumbnail_url($sticky1[0]->ID,'sn_big');
	// if (get_the_post_thumbnail_url($sticky1[0]->ID,'sn_big')) { $sticky1[0]->image = get_the_post_thumbnail_url($sticky1[0]->ID,'sn_big'); } else { $sticky1[0]['image'] = ''; }
	$postCategory = get_the_category($sticky1[0]->ID);
	$aux['name'] = $postCategory[0]->name;
	$aux['to'] = 'category/' . $postCategory[0]->slug;
	$sticky1[0]->category = $aux;

	$qSticky2 = array(
		'post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => 1,
		'meta_query'	=> array( 'relation' => 'OR', array( 'key' => 'sticky', 'value' => '2', 'compare' => 'LIKE' ), )
	);
	$sticky2 = get_posts($qSticky2);
	$GLOBALS['idReady'] = array_merge($GLOBALS['idReady'], wp_list_pluck( $sticky2, 'ID' ));
	$sticky2[0]->image = get_the_post_thumbnail_url($sticky2[0]->ID,'sn_normal');
	$postCategory = get_the_category($sticky2[0]->ID);
	$aux['name'] = $postCategory[0]->name;
	$aux['to'] = 'category/' . $postCategory[0]->slug;
	$sticky2[0]->category = $aux;

	$qSticky3 = array(
		'post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => 1,
		'meta_query'	=> array( 'relation' => 'OR', array( 'key' => 'sticky', 'value' => '3', 'compare' => 'LIKE' ), )
	);
	$sticky3 = get_posts($qSticky3);
	$GLOBALS['idReady'] = array_merge($GLOBALS['idReady'], wp_list_pluck( $sticky3, 'ID' ));
	$sticky3[0]->image = get_the_post_thumbnail_url($sticky3[0]->ID,'sn_normal');
	$postCategory = get_the_category($sticky3[0]->ID);
	$aux['name'] = $postCategory[0]->name;
	$aux['to'] = 'category/' . $postCategory[0]->slug;
	$sticky3[0]->category = $aux;

	array_push($stickies, $sticky1[0], $sticky2[0], $sticky3[0]);

	return $stickies;
}


function rankingsHome () {
	$qPopularPosts = array( 'post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => 3, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC' );
	$popularPosts = get_posts($qPopularPosts);
	foreach ($popularPosts as $post) {
		$post->image = get_the_post_thumbnail_url($post->ID,'sn_normal');
		$postCategory = get_the_category($post->ID);
		$aux['name'] = $postCategory[0]->name;
		$aux['to'] = 'category/' . $postCategory[0]->slug;
		$post->category = $aux;
	}
	return $popularPosts;
}

function postByCategoryHome () {
	$allCategoriesWithPosts = [];

	$categories = get_categories();

	foreach ($categories as $category) {
		$qPostByCategory = array( 'post_type' => 'post', 'category' => $category->term_id, 'post_status' => 'publish', 'post__not_in' => $GLOBALS['idReady'], 'posts_per_page' => 6 );
		$PostByCategory = get_posts($qPostByCategory);
		foreach ($PostByCategory as $post) { $post->image = get_the_post_thumbnail_url($post->ID,'sn_normal'); }

		$categorySection['category']['name'] = $category->name;
		$categorySection['category']['to'] = 'category/' . $category->slug;
		$categorySection['posts'] = $PostByCategory;

		array_push($allCategoriesWithPosts, $categorySection);
	}
	
	return $allCategoriesWithPosts;
}


function make_home () {
	
	$home = [];
  
	$home['stickies'] = allStickiesHome();
	$home['rankings'] = rankingsHome();
	$home['categories'] = postByCategoryHome();


 
  if ( empty( $home ) ) {
    return null;
  }
 
  return $home;
}



add_action( 'rest_api_init', function () {
  register_rest_route( 'snapi/v1', '/home', array(
    'methods' => 'GET',
    'callback' => 'make_home',
  ) );
} );












// POST FOR CATEGORY PAGE

function make_category_page ($data) {
	
	$paged = ( $data['page'] ) ? $data['page'] : 1;
	$categoryNews = get_posts(array('post_type'=>'post','post_status' => 'publish','category_name' => $data['slug'],'posts_per_page'=>12, 'paged'=>$paged));
	foreach ($categoryNews as $post) { $post->image = get_the_post_thumbnail_url($post->ID,'sn_normal'); }
	
 
  return $categoryNews;
}



add_action( 'rest_api_init', function () {
  register_rest_route( 'snapi/v1', '/sncategory/(?P<slug>[-\w]+)/(?P<page>\d+)', array(
    'methods' => 'GET',
    'callback' => 'make_category_page',
  ) );
} );











// SINGLE POST

function make_single_post ($data) {
	
	$singlePost = get_posts(array('post_type'=>'post','post_status' => 'publish','name' => $data['slug'],'posts_per_page'=>1));
	if( $singlePost ) {
		foreach ($singlePost as $post) { $post->image = get_the_post_thumbnail_url($post->ID,'full'); }
	} else {
		$singlePost = false;
	}
	
	
 
  return $singlePost;
}



add_action( 'rest_api_init', function () {
  register_rest_route( 'snapi/v1', '/snsingle/(?P<slug>[-\w]+)', array(
    'methods' => 'GET',
    'callback' => 'make_single_post',
  ) );
} );









// RANKINGS IN PAGE

function make_ranking ($data) {
	
	$singlePost = get_posts(array('post_type'=>'post','post_status' => 'publish','name' => $data['slug'],'posts_per_page'=>1));
	if( $singlePost ) {
		foreach ($singlePost as $post) { $post->image = get_the_post_thumbnail_url($post->ID,'full'); }
	} else {
		$singlePost = false;
	}
	
	
 
  return $singlePost;
}



add_action( 'rest_api_init', function () {
  register_rest_route( 'snapi/v1', '/snrankings', array(
    'methods' => 'GET',
    'callback' => 'rankingsHome',
  ) );
} );








// POST VIEWS

function wpb_set_post_views($postID) {
  $count_key = 'wpb_post_views_count';
  $count = get_post_meta($postID, $count_key, true);
  if($count==''){
    $count = 0;
    delete_post_meta($postID, $count_key);
    add_post_meta($postID, $count_key, '0');
  }else{
    $count++;
    update_post_meta($postID, $count_key, $count);
  }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);




// function wpb_track_post_views ($post_id) {
//   if ( !is_single() ) return;
//   if ( empty ( $post_id) ) {
//     global $post;
//     $post_id = $post->ID;    
//   }
//   wpb_set_post_views($post_id);
// }
// add_action( 'wp_head', 'wpb_track_post_views');



























































