<?php

if ( ! function_exists( 'sn_load_scripts' ) ) {
  function sn_load_scripts() {
    // For Performance
  	wp_dequeue_script('jquery');
    wp_dequeue_script('jquery-core');
    wp_dequeue_script('jquery-migrate');
    wp_enqueue_script('jquery', false, array(), false, true);
    wp_enqueue_script('jquery-core', false, array(), false, true);
    wp_enqueue_script('jquery-migrate', false, array(), false, true);
    // Local Assets
		wp_enqueue_script('appjs', get_template_directory_uri().'/assets/js/app.min.js', array('jquery'), '1.0.0', true);
 		wp_enqueue_style('maincss', get_template_directory_uri().'/assets/css/main.min.css');
  }
  add_action('wp_enqueue_scripts', 'sn_load_scripts');
}