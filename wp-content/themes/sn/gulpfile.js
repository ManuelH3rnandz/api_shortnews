'use strict';

const gulp = require("gulp");
const sass = require("gulp-sass");
const plumber = require("gulp-plumber");
const rename = require("gulp-rename");
const minify = require("gulp-minify-css");
const concat = require("gulp-concat");
const terser = require('gulp-terser');
const watch = require('gulp-watch');
 


gulp.task('css', function () {
    // Endless stream mode
    return gulp
    .src(['./node_modules/bootstrap/scss/bootstrap.scss', './src/style.css'])
    .pipe(sass())
    .pipe(plumber())
    .pipe(minify())
    .pipe(concat("main.min.css"))
    .pipe(gulp.dest('./assets/css'));
});

gulp.task('js', function () {
    // Endless stream mode
    return gulp
    .src(['./node_modules/bootstrap/dist/js/bootstrap.js', './src/app.js'])
    .pipe(plumber())
    //.pipe(terser())
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest('./assets/js'));
});

gulp.task('watch', function() {
  gulp.watch(['./node_modules/bootstrap/dist/js/bootstrap.js', './src/app.js'], ['js']);
  gulp.watch(['./node_modules/bootstrap/scss/bootstrap.scss', './src/style.css'], ['css']);
 });


// function css() {
//     return gulp
//     .src(['./node_modules/bootstrap/scss/bootstrap.scss', './src/style.css'])
//     .pipe(sass())
//     .pipe(plumber())
//     .pipe(minify())
//     .pipe(concat("main.min.css"))
//     .pipe(gulp.dest('./assets/css'));
// }
// function js() {
//     return gulp
//     .src(['./node_modules/bootstrap/dist/js/bootstrap.js', './src/app.js'])
//     .pipe(plumber())
//     //.pipe(terser())
//     .pipe(concat('app.min.js'))
//     .pipe(gulp.dest('./assets/js'));
// }


// exports.css = css;
// exports.js = js;